const Yup = require("yup")
const validateCreateTodo = function validateCreateTod(req, res, next) {
    Yup.object({
        text: Yup.string().required(),
        isCompleted: Yup.boolean().required()
    }).validate(req.body)
    .then(()=>next())
    .catch(err => res.status(400).json({ "message": err["message"] }));;
}

const validateUpdateTodo = function validateUpdateTodo(req, res, next){
    Yup.object({
        id: Yup.number().required(),
        text: Yup.string().required(),
        isCompleted: Yup.boolean().required()
    }).validate(req.body)
    .then(()=>next())
    .catch(err => res.status(400).json({ "message": err["message"] }));
} 

module.exports = { validateCreateTodo, validateUpdateTodo }