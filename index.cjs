const express = require("express");
const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient();
const { validateCreateTodo, validateUpdateTodo } = require("./validator/todoSchema")
const app = express();
app.use(express.json());

//returns list of todos
app.get('/todos', ( req, res) => {
    prisma.todos.findMany().then(data => {
        res.status(200).json(data);
    }).catch(err=>res.send(404))
});

//returns the todo matching the id
app.get('/todos/:Id', (req, res) => {

    const {Id} = req.params//extracting integer value from url
    let searchId = Number(Id);
    if(!Number.isInteger(searchId)){
        return res.send(500, "Invalid ID!")
    }
    prisma.todos.findUnique({
        where: {
            id: searchId,
        },
    }).then(data => {
        if (data === null) {
            return res.send(404);
        }else{
            res.status(201).json(data);
        }
    }).catch(err => res.status(500).json(err))
})

//delete the todo matching the id
app.delete('/todos/:Id', (req, res) => {
    const {Id} = req.params//extracting integer value from url
    let searchId = Number(Id);
    if(!Number.isInteger(searchId)){
        return res.send(500, "Invalid ID!");
    }
    prisma.todos.delete({
        where: {
            id: searchId
        },
    }).then(data => { 
        res.status(200).json(data)
    }).catch(err => res.status(404).json({ "message": "ID not found!" }))
});

//creates a new todo
app.post('/todos',validateCreateTodo, (req, res) => {
    prisma.todos.create({
        data: req.body
    }).then((data) => res.status(201).json(data)).catch(err=>res.send(500, err))
})


//updates the todo matching the id
app.put('/todos',validateUpdateTodo,(req, res) => {

    prisma.todos.update({
        where: {
            id: parseInt(req.body.id)
        },
        data: {
            text: req.body.text,
            isCompleted: req.body.isCompleted
        },
    }).then((data) => {

        if (data === null) {
            res.status(404)
        }
        res.status(200).json(data)
    }).catch(err => res.send(404,"Not found!"))

});

app.listen(3000)