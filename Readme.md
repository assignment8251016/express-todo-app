# Setting up an Express Todo App

## Development Instructions

### Prerequisites

Install the below mentioned application to run any Node application

* Node.js

After Installing the Node application

# steps

#### 1. Install required dependencies

Open the project root directory and run 
```bash
npm install
```
This will install all the libraries required for runing the application.

#### 2. Initialize the Prisma 
```bash
npx prisma init
```
This will initialize the project for Prisma by creating a directory `prisma` and `.env` file that has the Postgres dummy url in the root of the project. Don't forget to change the placeholders accordingly.
DATABASE_URL=postgresql://<username>:<password>@<hostname>:<port>/<database_name>


#### 3. Create a Prisma schema
This will create the schema from pre-existing Database on your Postgres server
```bash
`npx prisma db pull`
``` 

### 4. Generate Prisma client to connect to your Database

In your terminal, run the following command to generate the Prisma client based on your schema:

```bash
npx prisma generate
```
This will create a new node_modules/@prisma/client directory containing the Prisma client.

### 5. Add your Prisma client in index.js

Just copy the all line from terminal after doing the step 5 and paste them in starting of your Express App.
```
const PrismaClient = require('@prisma/client')
const prisma = new PrismaClient()

```
### 6. To start the application

```bash
npm start
```

### API 
The API accepts and returns JSON data. Here are the request and response formats for each endpoint:

#### GET /todos:

Request: None
Response: Array of todo objects.

#### GET /todos/:id

Request: None
Response: Todo object.

#### POST /todos: 

Request: Todo object (in the request body).
Response: Created todo object.

#### PUT /todos:

Request: Updated todo object (in the request body).
Response: Updated todo object.

#### DELETE /todos/:id

Request: None
Response: Deleted todo object.
